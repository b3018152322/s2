package com.zuitt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class WDC043_S2_A2 {
    public static void main(String args[]){
        System.out.println("Activity for Arrays");

        int[] numArray = new int[5];

        numArray[0] = 2;
        numArray[1] = 3;
        numArray[2] = 5;
        numArray[3] = 7;
        numArray[4] = 11;

        System.out.println("The updated array is shown below: ");
        System.out.println(Arrays.toString(numArray));

        System.out.println("The first prime number is: " + numArray[0]);
        System.out.println("The second prime number is: " + numArray[1]);
        System.out.println("The third prime number is: " + numArray[2]);
        System.out.println("The fourth prime number is: " + numArray[3]);
        System.out.println("The fifth prime number is: " + numArray[4]);

        System.out.println("-----------------------------------------");
        System.out.println("Activity for ArrayList");

        ArrayList<String> friends = new ArrayList<String>();

        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");

        System.out.println("My friends are: " + friends);

        System.out.println("-----------------------------------------");
        System.out.println("Activity for HashMaps");

        HashMap<String, Integer> inventory_qty = new HashMap<String, Integer>();

        inventory_qty.put("toothpaste", 15);
        inventory_qty.put("toothbrush", 20);
        inventory_qty.put("soap", 12);

        System.out.println("Our current inventory consists of: " + inventory_qty);

    }
}
