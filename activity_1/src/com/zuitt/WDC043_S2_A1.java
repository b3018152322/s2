package com.zuitt;

import java.util.Scanner;

public class WDC043_S2_A1 {

    public static void main(String args[]) {
        Scanner yearChecker = new Scanner(System.in);

        System.out.println("Please input a year to check if it is a leap year: ");
        int year = Integer.parseInt(yearChecker.nextLine());

        if (year > 0) {
            int leapYear = year % 4;
            int div100 = year % 100;
            int div400 = year % 400;

            if (leapYear == 0) {

                if (div100 == 0) {
                    if (div400 == 0) {
                        System.out.println(year + " is a leap year!");
                    } else {
                        System.out.println(year + " is NOT a leap year!");
                    }
                }
                else {
                    System.out.println(year + " is a leap year!");
                }

            }
            else {
                System.out.println(year + " is NOT a leap year!");
            }


        }
        else {
            System.out.println("Invalid year!");
        }

    }
}
